set encoding=utf-8
set list listchars=nbsp:¬,tab:»·,trail:·,extends:>
syntax enable
set smartindent
set shiftwidth=2
set softtabstop=2
set tabstop=2
set expandtab
highlight lineNr ctermfg=white cterm=italic

syntax enable
highlight Search ctermbg=12

:autocmd BufWritePost *.tex !xelatex <afile>
