vim.o.encoding = 'utf-8'

vim.cmd [[set list listchars=nbsp:¬,tab:»·,trail:·,extends:>]]
vim.cmd [[highlight lineNr ctermfg=white cterm=italic]]

-- vim.o.smartindent = true
-- vim.o.shiftwidth = 4
-- vim.o.softtabstop = 4
-- vim.o.tabstop = 4
-- vim.o.expandtab = true

vim.opt.nu = true
vim.opt.relativenumber = true

vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

vim.opt.smartindent = true

vim.opt.wrap = false

vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

vim.opt.hlsearch = true
vim.opt.incsearch = true

vim.opt.termguicolors = true

vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"
vim.opt.isfname:append("@-@")

vim.opt.updatetime = 50

vim.opt.colorcolumn = "80"

vim.cmd ('colorscheme rose-pine')
vim.cmd [[highlight Search ctermbg=12]]
vim.cmd [[:autocmd BufWritePost *.tex !xelatex <afile>]]
