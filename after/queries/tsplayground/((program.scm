((program
  (variable_declarator
    _ (identifier) @variable_name
    _ @assignment
    (identifier) @_function_name))
  (#set! @assignment (= _ @variable_name)))
